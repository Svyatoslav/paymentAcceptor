name := "paymentAcceptor"

version := "1.0"

lazy val `paymentacceptor` = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq( jdbc , cache , ws   , specs2 % Test, "com.typesafe.akka" % "akka-http-experimental_2.11" % "2.4.6",
  "com.typesafe.akka" %% "akka-slf4j" % "2.4.6")

unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )  

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"  