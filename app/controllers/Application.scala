package controllers

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpCharsets, HttpEntity, _}
import com.typesafe.sslconfig.akka.AkkaSSLConfig
import play.api.libs.json.Json
import play.api.mvc._
import services.appService.bodys

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

object Application extends Controller {

  def index = Action {
    Ok(views.html.index.render(bodys))
  }

  def indexPost = Action { implicit req =>
    println(req.remoteAddress)
    println(req.headers)
    bodys += req.body
    val incParams = req.body.asFormUrlEncoded.get
    val params = "cmd=_notify-validate&" + incParams.toList.mkString("&")
    println("send params: " + params)
    println("inc body: " + req.body)

//    WS.url("https://www.sandbox.paypal.com/cgi-bin/webscr")
//      .withHeaders("Content-Type" -> "application/x-www-form-urlencoded")
//      .post(params)
//      .map { resp => println(resp.status); println(resp.body) }

    val ct = ContentType(MediaTypes.`application/x-www-form-urlencoded`, HttpCharsets.`UTF-8`)
    val entity = HttpEntity(ct, params)


    implicit val actorSystem = ActorSystem("new")
    implicit val materializer = akka.stream.ActorMaterializer()(actorSystem)
    val httpsContext = Http().createClientHttpsContext(AkkaSSLConfig(actorSystem))

    val q = Await.result(Http().singleRequest(HttpRequest(HttpMethods.POST, Uri("https://www.sandbox.paypal.com/cgi-bin/webscr"), Nil, entity), httpsContext).flatMap {
      response => response.entity.toStrict(5.second).map(_.data.utf8String).map {
        case s if s == "VERIFIED" => "ok"
        case s => s"bad: $s"
      }
    }, Duration.Inf)

    println(s"RESULT: $q")


    Ok(Json.obj("" -> ""))
  }

}
